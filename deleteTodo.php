<?php

    require_once 'connection.php';

    if(isset($_POST['todo_id'])) {

        $id = $_POST['todo_id'];

        $query = $mysqli->query("DELETE FROM `todos` WHERE `id` = $id");

        if($query) {

            $response = ['status' => 'success', 'message' => 'Success in deleting todo #'. $id];

            echo json_encode($response);
        }

        exit();
    }
