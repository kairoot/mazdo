// Initialization
const completedTaskList = 'completed-tasks';
const pendingTaskList = 'pending-tasks';

loadTasks();


// When the user hits enter.
document.getElementById('todo-text').addEventListener('keyup', addNewTodo);
document.addEventListener('keyup', updateTodo);


function addNewTodo(event) {
    event.preventDefault();

    if(event.keyCode === 13) {
        var todo = getTodoValue();

        if(todo !== '') {
            // console.log(todo);
            sendServerData(todo);
            // console.log(data);
            clear();
        } else {
            // alert('Error you did not fill up the textbox!');
            console.error('Error please fill up the textbox!');
        }
    }
}

function getTodoValue() {
    // Gets the input #todo-text value
    return document.getElementById('todo-text').value;
}

function clear() {
    document.getElementById('todo-text').value = "";
}

function sendServerData(todo) {

    var xhr = new XMLHttpRequest();

    xhr.open('POST', 'addTodo.php', true);

    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    xhr.onload = function() {
        if(this.status == 200) {
            var response = JSON.parse(this.responseText);
            appendTodos(false, pendingTaskList, [ response.data.todo ]);
        }
    }

    xhr.send('todo=' + todo);
}

function loadTasks() {
    var xhr = new XMLHttpRequest();

    xhr.open('GET', 'getTodos.php', true);

    xhr.onload = function() {
        if(this.status == 200) {
            // console.log(this.statusText);
            var response = JSON.parse(this.responseText);

            // Adding completed todos.
            appendTodos(true, completedTaskList, response.tasks.completed);

            // Adding pending todos.
            appendTodos(false, pendingTaskList, response.tasks.pending);
        }
    }

    xhr.send();
}

function appendTodos(isCompleted = false, targetList, todos) {

    var domString = getDOMString(isCompleted, todos);
    drawTodoListDOM(targetList, domString);
}

function getDOMString(isCompletedTasks = false, todos) {
    var output = '';

    for(var i in todos) {
        output += '<div class="row" id="todo-'+ todos[i].id +'">';
        output += '<div class="col-sm-2">';
        /*
            output += '<div class="form-check">';
            output += '<label class="form-check-label">';

            if(isCompletedTasks == false)
                output += '<input type="checkbox" class="form-check-input" value="0" onchange="changeTodoStatus('+ todos[i].id +',\''+todos[i].title +'\' , 1)">';
            else
                output += '<input type="checkbox" class="form-check-input" value="1" onchange="changeTodoStatus('+ todos[i].id +',\''+todos[i].title +'\' , 0)" checked>';

            output += '</label>';
            output += '</div>';
        */
        output += '</div>';

        output += '<div class="col-sm-8">';
        output += '<div class="form-group">';
        if(isCompletedTasks == false)
            output += '<input type="checkbox" class="form-check-input todo-checkbox" value="0" onchange="changeTodoStatus('+ todos[i].id +',\''+todos[i].title +'\' , 1)">';
        else
            output += '<input type="checkbox" class="form-check-input todo-checkbox" value="1" onchange="changeTodoStatus('+ todos[i].id +',\''+todos[i].title +'\' , 0)" checked>';
        output += '<input type="text" class="form-control todo-input-text" name="todo" value="'+ todos[i].title +'" data-id="'+todos[i].id+'">';
        output += '</div>';
        output += '</div>';

        output += '<div class="col-sm-2">';
        output += '<a href="javascript:;" onclick="deleteTodo('+ todos[i].id +')" class="btn-remove-todo text-danger"> <i class="fas fa-times"></i> </a>';
        output += '</div>';
        output += '</div>';
    }

    return output;
}

function drawTodoListDOM(targetList, htmlDOMString) {

    var todoList = document.getElementById(targetList);
    todoList.insertAdjacentHTML('beforeend', htmlDOMString);
}

function deleteTodo(id) {

    var xhr = new XMLHttpRequest();

    xhr.open('POST', 'deleteTodo.php', true);

    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    xhr.onload = function() {
        if(this.status == 200) {
            // console.log(this.statusText);
            var response = JSON.parse(this.responseText);
            document.getElementById('todo-' + id).remove();
            console.log(response);
        }
    }

    xhr.send('todo_id=' + id);
}

function changeTodoStatus(id, title = "" ,isCompleted = false) {

    var xhr = new XMLHttpRequest();

    xhr.open('POST', 'setTodoStatus.php', true);

    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    xhr.onload = function() {
        if(this.status == 200) {
            // console.log(this.statusText);
            var response = JSON.parse(this.responseText);
            document.getElementById('todo-' + id).remove();
            var todo = {
                id: id,
                title: title,
                status: isCompleted
            };

            if(isCompleted)
                appendTodos(true, completedTaskList, [todo]);
            else
                appendTodos(false, pendingTaskList, [todo]);
        }
    }

    xhr.send('todo_id=' + id + '&status=' + isCompleted);
}

function updateTodo(event) {
    if(event.keyCode === 13 && event.target.classList.contains( 'todo-input-text' )) {

        var todoText = event.target;
        var id = event.target.getAttribute('data-id');

        var xhr = new XMLHttpRequest();

        xhr.open('POST', 'updateTodo.php', true);

        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        xhr.onload = function() {
            if(this.status == 200) {
                // console.log(this.statusText);
                var response = JSON.parse(this.responseText);
                // console.log(response);

            }
        }

        xhr.send('todo_id=' + id + '&title=' + todoText.value);
    }
}
