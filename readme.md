# MUZDO

Muzdo is a simple clone of Google Keep, which uses the following technologies:
  - Bootstrap 4
  - Vanilla PHP
  - Vanilla Javascript 

# Features!

  - Add new task
  - Delete a task 
  - Update a task 
  - View task list
  
You can also:
  - Check or unchecked a task!

### Tech

MUZDO uses a number of open source projects to work properly:

* [Bootstrap 4] - great UI boilerplate for modern web apps
* [Vanilla PHP] - a cool PHP framework!
* [Vanilla Javascript] - a cool Javascript framework!
* [XHR] - Maybe in the next update i will try to use newer ways.

And of course MUZDO itself is open source with a [public repository][dill]
 on Gitlab.

### Installation

Muzdo requires the following to run in your machine : 
* PHP 7 
* Mysql
* Apache Server
* Browser

Download or clone this project. 

Open phpmyadmin and import the database.sql file. 

Navigate to this url: http://localhost/muzdo/

Thats it! Thank you :) 
