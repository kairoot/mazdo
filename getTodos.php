<?php
    require_once 'connection.php';

    $query = $mysqli->query("SELECT * FROM `todos`");

    $pending_tasks = [];
    $completed_tasks = [];

    while($task = $query->fetch_object())
    {
        if($task->status == 1)
            $completed_tasks[] = $task;
        else
            $pending_tasks[] = $task;
    }

    $tasks = [
        'pending' => $pending_tasks,
        'completed' => $completed_tasks
    ];

    echo json_encode(compact('tasks'));
    exit();
