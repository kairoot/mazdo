<?php

    require_once "connection.php";

    if(isset($_POST['todo_id']))
    {
        $title = $mysqli->real_escape_string($_POST['title']);
        $id = $_POST['todo_id'];

        $query = $mysqli->query("UPDATE `todos` SET `title`='$title' WHERE `id` = '$id'");

        $data = [
            'result' => 'error',
            'message' => 'Cannot update todo.',
            'todo' => null
        ];

        if($query)
        {

            $data = [
                'result' => 'success',
                'message' => 'Success in updating todo.',
                'todo' => ['id' => $id, 'title' => $title]
            ];
        }

        echo json_encode(compact('data'));
        exit();
    }
