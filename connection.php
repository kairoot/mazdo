<?php
    $host = "localhost";
    $username = "root";
    $password = "";
    $database = "muzdo";

    $mysqli = new mysqli($host, $username, $password, $database);

    if($mysqli->connect_errno) {
        echo "Failed to establish connection. Error : ".$mysqli->connect_error;
    }
