<?php

    require_once "connection.php";

    if(isset($_POST['todo']))
    {
        $title = $mysqli->real_escape_string($_POST['todo']);

        $query = $mysqli->query("INSERT INTO `todos`(`title`, `status`) VALUES ('$title', 0)");

        $data = [
            'result' => 'error',
            'message' => 'Cannot add new todo.',
            'todo' => null
        ];

        if($query)
        {
            $id = $mysqli->insert_id;

            $data = [
                'result' => 'success',
                'message' => 'Success in adding new todo.',
                'todo' => ['id' => $id, 'title' => $title, 'status' => 0]
            ];
        }

        echo json_encode(compact('data'));
        exit();
    }
